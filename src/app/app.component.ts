import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'connect-four';

  endgame = false;
  winner;

  endGameReached(e){
    this.endgame = e[0];
    if(e[1] == true){
      this.winner = "Rood";
    }else{
      this.winner = "Blauw";
    }
  }

}
