import { PlayerbotService } from './playerbot.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { GridComponent } from './grid/grid.component';
import { SpaceComponent } from './space/space.component';
import { CoinComponent } from './coin/coin.component';
import { EndgameComponent } from './endgame/endgame.component';

@NgModule({
  declarations: [
    AppComponent,
    GridComponent,
    SpaceComponent,
    CoinComponent,
    EndgameComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [PlayerbotService],
  bootstrap: [AppComponent]
})
export class AppModule { }
