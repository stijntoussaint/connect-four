import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'coin',
  templateUrl: './coin.component.html',
  styleUrls: ['./coin.component.css']
})
export class CoinComponent implements OnInit {

  @Input() coinSymbol;
  coinColor;

  constructor() { 
  }

  ngOnInit(): void {
    if(this.coinSymbol == "b"){
      this.coinColor = true;
    }else{
      this.coinColor = false;
    }
  }

}
