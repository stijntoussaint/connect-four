import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'endgame',
  templateUrl: './endgame.component.html',
  styleUrls: ['./endgame.component.css']
})
export class EndgameComponent implements OnInit {

  @Input() winner;

  constructor() { }

  ngOnInit(): void {
  }

  playAgain(){
    location.reload();
  }

}
