import { PlayerbotService } from './../playerbot.service';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.css']
})
export class GridComponent implements OnInit {

  grid;
  sizeOfPlayingField;
  spaceSize;
  spaces;
  turn;
  bot;

  @Output() endGameReached = new EventEmitter<object>();;

  bot;
  constructor(bot : PlayerbotService){
    this.bot = bot;
  }

  ngOnInit(): void {
    this.sizeOfPlayingField = 7;
    this.spaceSize = 50;

    this.turn = false;
    //Initialize grid
    this.grid = new Array(this.sizeOfPlayingField);
    for(let i = 0; i < this.grid.length; i++){
      this.grid[i] = new Array(this.grid.length);
    }
    //Fill grid with _ to represent empty spaces
    for(let x = 0; x < this.grid.length; x++){
      for(let y = 0; y < this.grid.length; y++){
        this.grid[x][y] = "_";
      }
    }

    //Set space width and height
    //Can't get this to work.. would be nice
    // var spaces = document.getElementsByClassName("space");
    // for(var i = 0; i < spaces.length; i++) { 
    //   var element = spaces[i];
    //   spaces[i].setAttribute("width", this.spaceSize+'px');
    //   spaces[i].setAttribute("height", this.spaceSize+'px');
    // }
    this.dropCoin(this.bot.play(this.grid));
  }

  dropCoin(x){
    //Loop through column
    for(let y = 0; y < this.grid[x].length; y++){
      let thisCoin = this.grid[x][y];
      let belowCoin = this.grid[x][y+1];
      //If the upper most space is not empty return
      if(thisCoin == "b" || thisCoin == "r"){
        return;
        //Check if this space is empty and below space is not empty or undefined
      }else if(thisCoin == "_" && belowCoin != "_"){
        if(this.turn == true){
          this.grid[x][y] = "b";
        }else{
          this.grid[x][y] = "r";
        }
        this.turn = !this.turn;

        //Check for win and let bot play
        if(this.checkWin(x, y) == false && this.turn == false){
          console.log("bot play");
          // console.log(this.bot.checkWin(this.grid, 3, 3, "r"));
          this.dropCoin(this.bot.play(this.grid));
        }
        return;
      }
    }
  }

  checkWin(x, y){
    let win = 4;
    let comboCount = 1;

    //Check for win in column
    for(let i = 0; i < this.grid[x].length; i++){
      //Loop through length of column
      let prevSpace;
      let currentSpace = this.grid[x][i];
      if(i > 0){
        prevSpace = this.grid[x][i-1];
      }else{
        //If out of bounds act as if its empty
        prevSpace = "_";
      }

      //If currentSpace is empty break combo
      if(currentSpace != "_"){
        if(currentSpace == prevSpace){
          comboCount++;
        }else{
          comboCount = 1;
        }
      }

      if(comboCount >= win){
        this.endGameReached.emit(
          [true,
            this.turn]
        );
        return true;
      }
    }


    //Check for win in row
    for(let i = 0; i < this.grid[x].length; i++){
      let prevSpace;
      //Loop through row
      let currentSpace = this.grid[i][y];
      if(i > 0){
        prevSpace = this.grid[i-1][y];
      }else{
        prevSpace = "_";
      }
      if(currentSpace != "_"){
        if(currentSpace == prevSpace){
          comboCount++;
        }else{
          comboCount = 1;
        }
      }

      if(comboCount >= win){
        this.endGameReached.emit(
          [true,
            this.turn]
        );
        return true;
      }
    }

    //Check for win in diagonal: topleft -> bottomright
    let beginX = x;
    let beginY = y;
    //Get the most upper left coordinates in diagonal
    while(beginX > 0 && beginY > 0){
      beginX--;
      beginY--;
    }

    let countX = beginX;
    let countY = beginY;
    //Keep looping until countX or countY finds a boundary
    while(countX < this.grid[x].length && countY < this.grid[x].length){
      let prevSpace;
      let currentSpace = this.grid[countX][countY];
      if(countX > 0 && countY > 0){
        prevSpace = this.grid[countX-1][countY-1];
      }else{
        prevSpace = "_";
      }
      if(currentSpace != "_"){
        if(currentSpace == prevSpace){
          comboCount++;
        }else{
          comboCount = 1;
        }
      }

      if(comboCount >= win){
        this.endGameReached.emit(
          [true,
            this.turn]
        );
        return true;
      }
      countX ++;
      countY ++;

    }

    //Check for win in diagonal: bottomleft -> topright
    beginX = x;
    beginY = y;
    //Get the most bottom left coordinate in diagonal
    while(beginX > 0 && beginY < this.grid[x].length){
      beginX--;
      beginY++;
    }

    countX = beginX;
    countY = beginY;
    while(countX < this.grid[x].length && countY > 0){
      let prevSpace;
      let currentSpace = this.grid[countX][countY];
      if(countX > 0 && countY < this.grid[x].length){
        prevSpace = this.grid[countX-1][countY+1];
      }else{
        prevSpace = "_";
      }
      if(currentSpace != "_"){
        if(currentSpace == prevSpace){
          comboCount++;
        }else{
          comboCount = 1;
        }
      }

      if(comboCount >= win){
        this.endGameReached.emit(
          [true,
          this.turn]
        );
        return true;
      }
      countX ++;
      countY --;

    }

    return false;

  }

}
