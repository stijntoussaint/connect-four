import { TestBed } from '@angular/core/testing';

import { PlayerbotService } from './playerbot.service';

describe('PlayerbotService', () => {
  let service: PlayerbotService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PlayerbotService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
