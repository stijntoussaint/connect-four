import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PlayerbotService {
  constructor() { 
    console.log(this);
  }

  play(grid){
    //Bot is red -> r
    //Player is blue -> b 
    let score = 0;

    //Playable positions
    let playScores = new Array(grid[0].length);

    //Look at all rows
    for(let x = 0; x < grid[0].length; x++){
      let playablePosition = -1;
      //Look for playable position in row
      for(let y = 0; y < grid[0].length; y++){
        if(grid[x][y] == "_"){
          playablePosition = y;
        }
      }

      score = 0;
      var nonPlayableCols = new Array();
      //If position is playable
      if(playablePosition >= 0){
        if(x == 3){
          score += 4;
        }
        //Look at all directions for win and lose and assign scores
        if(this.checkWin(grid, x, playablePosition, "r", 4)){
          score += 2000;
        }
        if(this.checkWin(grid, x, playablePosition, "b", 4)){
          score += 500;
        }

        //Look at all directions for 2 in a row and assign scores
        if(this.checkWin(grid, x, playablePosition, "r", 2)){
          score += 20;
        }
        if(this.checkWin(grid, x, playablePosition, "b", 2)){
          score += 15;
        }

        //Look at all directions for 3 in a row and assign scores
        if(this.checkWin(grid, x, playablePosition, "r", 3)){
          score += 200;
        }
        if(this.checkWin(grid, x, playablePosition, "b", 3)){
          score += 125;
        }

        //Store values
        playScores[x] = score;
      }else{
        //Save non playable cols
        nonPlayableCols.push(x);
        playScores[x] = 0;
      }
    }


    let play;

    //Make all numbers positive
    let maxScore = Math.abs(playScores[0]);
    let maxIndex = 0;
    //Search for highest number
    for (var i = 0; i < playScores.length; i++) {
      let absPlayScore = Math.abs(playScores[i]);
      console.log(i+" "+absPlayScore);
        if (absPlayScore > maxScore) {
            maxIndex = i;
            maxScore = absPlayScore;
        }
    }

    //If highest number is higher than 0 play number
    if(maxScore > 0){
      play = maxIndex;
    }else{
      //Generate random column number that is playable
      let n = -1;
      while(n == -1 || nonPlayableCols.includes(n)){
        n = Math.floor(Math.random() * 7);
      };
      play = n;
    }  
    return play;
  }

  checkWin(grid, x, y, player, combo){
    let comboCount = 1;
    let win = combo;
    //Initialize testRow
    let testRow = new Array(grid[x].length);
    for(let i = 0; i < testRow.length; i++){
      testRow[i] = grid[i][y];
    }
    //Inputs possible coin into row
    testRow[x] = player;
    //Check along x axis
    for(let i = 0; i < testRow.length; i++){
      let prevSpace;
      //Loop through row
      let currentSpace = testRow[i];
      if(i > 0){
        prevSpace = testRow[i-1];
      }else{
        prevSpace = "_";
      }
      //If current space is player and is the same as space before
      if(currentSpace == player){
        if(currentSpace == prevSpace){
          comboCount++;
        }else{
          comboCount = 0;
        }
      }

      //If it is a winning row
      if(comboCount >= win){
        console.log(player+" x-axis "+comboCount);

        return true;
      }
    }
    

    for(let i = 0; i < testRow.length; i++){
      testRow[i] = grid[x][i];
    }
    testRow[y] = player;
    //Check along Y axis
    for(let i = 0; i < testRow.length; i++){
      let prevSpace;
      //Loop through row
      let currentSpace = testRow[i];
      if(i > 0){
        prevSpace = testRow[i-1];
      }else{
        prevSpace = "_";
      }
      if(currentSpace == player){
        if(currentSpace == prevSpace){
          comboCount++;
        }else{
          comboCount = 1;
        }
      }

      if(comboCount >= win){
        console.log(player+" y-axis "+comboCount);
        return true;
      }
    }

    //Check for win in diagonal: topleft -> bottomright
    let beginX = x;
    let beginY = y;
    //Get the most upper left coordinates in diagonal
    while(beginX > 0 && beginY > 0){
      beginX--;
      beginY--;
    }

    let countX = beginX;
    let countY = beginY;
    let index = 0;
    while(countX < grid[x].length && countY < grid[x].length){
      if(countX != x && countY != y){
        testRow[index] = grid[countX][countY];
      }else{
        testRow[index] = player;
      }
      index ++;
      countX ++;
      countY ++;
    }
    countX = beginX;
    countY = beginY;
    //Keep looping until countX or countY finds a boundary
    for(let i = 0; i < index; i++){
      let prevSpace;
      //Input possible coin into row
      let currentSpace = testRow[i];
      if(countX > 0 && countY > 0){
        prevSpace = testRow[i-1];
      }else{
        prevSpace = "_";
      }
      if(currentSpace == player){
        if(currentSpace == prevSpace){
          comboCount++;
        }else{
          comboCount = 1;
        }
      }

      if(comboCount >= win){
        console.log(player+" topleft->bottomRight "+comboCount);
        return true;
      }
      countX ++;
      countY ++;

    }

    //Check for win in diagonal: bottomLeft -> topright
    beginX = x;
    beginY = y;
    //Get the most upper left coordinates in diagonal
    while(beginX > 0 && beginY < grid[x].length){
      beginX--;
      beginY++;
    }

    countX = beginX;
    countY = beginY;
    index = 0;
    while(countX < grid[x].length && countY > -1){
      if(countX != x && countY != y){
        testRow[index] = grid[countX][countY];
      }else{
        testRow[index] = player;
      }
      index ++;
      countX ++;
      countY --;
    }
    countX = beginX;
    countY = beginY;
    //Keep looping until countX or countY finds a boundary
    for(let i = 0; i < index; i++){
      let prevSpace;
      let currentSpace = testRow[i];
      if(countX > 0 && countY > 0){
        prevSpace = testRow[i-1];
      }else{
        prevSpace = "_";
      }
      if(currentSpace == player){
        if(currentSpace == prevSpace){
          comboCount++;
        }else{
          comboCount = 1;
        }
      }

      if(comboCount >= win){
        console.log(player+" bottomLeft->topright "+comboCount);
        return true;
      }
      countX ++;
      countY ++;

    }

    return false;

  }
}
